import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Answer, Question } from '../../shared/question.model';
import { QuestionService } from '../../shared/question.service';

@Component({
  selector: 'app-question-item',
  templateUrl: './question-item.component.html',
  styleUrls: ['./question-item.component.css']
})
export class QuestionItemComponent {
  @Input() question!: Question;
  showHelp = false;


  constructor(private questionService: QuestionService){}


}
