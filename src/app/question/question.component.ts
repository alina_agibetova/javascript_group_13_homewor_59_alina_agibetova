import { Component, Input, OnInit } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { Answer, Question } from '../shared/question.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  questions!: Question[];



  constructor(private questionService: QuestionService) { }

  ngOnInit(): void {
    this.questions = this.questionService.getQuestions();

  }



}
