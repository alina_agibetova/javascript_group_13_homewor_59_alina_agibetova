import { Component, Input } from '@angular/core';
import { QuestionService } from '../shared/question.service';
import { Answer } from '../shared/question.model';


@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent {
  @Input()answers!: Answer[];

  constructor(private questionService: QuestionService){}

  ngOnInit(){
    this.answers = this.questionService.getAnswer();
    this.questionService.answerChange.subscribe((answer: Answer[]) => {
      this.answers = answer;
    });


  }
}
