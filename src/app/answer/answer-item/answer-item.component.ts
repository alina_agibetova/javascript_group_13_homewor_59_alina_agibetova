import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { QuestionService } from '../../shared/question.service';
import { Answer, Question } from '../../shared/question.model';


@Component({
  selector: 'app-answer-item',
  templateUrl: './answer-item.component.html',
  styleUrls: ['./answer-item.component.css']
})
export class AnswerItemComponent {
  @ViewChild('answerInput') answerInput!: ElementRef;
  @Input() answer!: Answer;
  @Input() question!: Question;


  constructor(private questionService: QuestionService){}


  OnSubmit() {
    const name = this.answerInput.nativeElement.value;
    const item = new Answer(name);
    this.questionService.addAnswer(item);
    console.log(name);
  }

}
