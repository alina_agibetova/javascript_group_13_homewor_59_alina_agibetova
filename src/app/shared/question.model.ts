export class Question {
  constructor(
    public questionName: string,
    public answerName: string,
    public help: string,
    public answerStatus: string,
  ){}
}

export class Answer {
  constructor(
    public answerName: string,
  ){}
}

