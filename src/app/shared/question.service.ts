import { EventEmitter } from '@angular/core';
import { Answer, Question } from './question.model';


export class QuestionService {
  answerChange = new EventEmitter<Question[]>();
  answer!: Answer;
  private questions: Question[] = [
    new Question('1. Все соседушки Живут рядышком, А свидеться не могут', 'Окна', 'Неодушевленный предмет', 'Ответ верный'),
    new Question('2. Темный Ивашка, из дерева рубашка,Там где носом проведет,Там заметочку кладет.', 'Карандаш', 'Неодушевленный предмет', 'Ответ верный'),
    new Question('3. Сам он легкий и худой,Но с тяжеленной головой.', 'Молоток', 'Неодушевленный предмет', 'Ответ верный'),
  ];

  private answers: Answer[] = [
    new Answer('окна'),
    new Answer('карандаш'),
    new Answer('молоток'),
  ];


  getQuestions(){
    return this.questions.slice();
  }

  getAnswer(){
    return this.answers.slice();
  }


  addAnswer(answer: Answer){
    this.answers.push(answer);
    this.answerChange.emit(this.questions);
    console.log(answer);
  }

}
