import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { AnswerComponent } from './answer/answer.component';
import { QuestionService } from './shared/question.service';
import { FormsModule } from '@angular/forms';
import { QuestionItemComponent } from './question/question-item/question-item.component';
import { AnswerItemComponent } from './answer/answer-item/answer-item.component';
import { AnswerDirective } from './directives/answer.directive';

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    AnswerComponent,
    QuestionItemComponent,
    AnswerItemComponent,
    AnswerDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
