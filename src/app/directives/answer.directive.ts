import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector:'[appIsTrue]'
})

export class AnswerDirective {
  constructor(private el: ElementRef){
    el.nativeElement.style.background = 'lightgreen';
  }

}

